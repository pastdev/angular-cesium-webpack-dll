// This is as suggested at:
// https://angular.io/docs/ts/latest/guide/webpack.html#!#polyfills
// so we will ignore the following lint...
/* tslint:disable:no-var-requires no-string-literal */
import "core-js/es6";
import "core-js/es7/reflect";
require("zone.js/dist/zone");

if (process.env.ENV === "production") {
  // Production
} else {
  // Development and test
  Error["stackTraceLimit"] = Infinity;
  require("zone.js/dist/long-stack-trace-zone");
}
